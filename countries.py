"""World university ranks dataset."""

import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

# %%
# loading the data
data = pd.read_csv('~/data/university_rank.csv')
data.head(10)
data.isnull().sum()
data.nunique()
data.shape
data = data.drop(['broad_impact'], axis='columns')
data['national_rank'].max()
data['world_rank'].max()

# %%
# No of educational institutions present
data['institution'].nunique()  # 1024

# %%
# No of countries present
data['country'].nunique()

# %%
# no of universities present in each country
data1 = (data.groupby('country')['institution'].count()
             .sort_values(ascending=False)).head(10)
plt.xticks(rotation=90)
sns.barplot(x=data1.index, y=data1.values)
plt.ylabel('No of universities')
plt.title('No of universities in each country')

# %%
# Top 10 world ranked universities in yeach year()
data3 = data[data['world_rank'] <= 10][['year', 'institution']]
data3

# %%
# which universites got the score above 90
data4 = data[data['score'] > 90]
data4['institution'].unique()

# %%
# which university got the full score in each year
data5 = data[data['score'] == 100]
data5[['year', 'institution']]

# %%
#  No of Universities present in India
data6 = data[data['country'] == 'India']
data6['institution'].nunique()

# %%
# How does the rank of some universities varies each year
data7 = (data[data['institution'] == 'Stanford University']
             [['year', 'world_rank']])
data8 = (data[data['institution'] == 'Harvard University']
             [['year', 'world_rank']])
data9 = (data[data['institution'] == 'University of Cambridge']
             [['year', 'world_rank']])
y1 = data7['world_rank']
y2 = data8['world_rank']
y3 = data9['world_rank']
x = data['year']
x1 = x.unique()
x2 = pd.DataFrame(x1)
plt.xticks([2012, 2013, 2014, 2015])
plt.yticks([1, 2, 3, 4, 5])
plt.plot(x2, y1, 'r', marker='*', label='Stanford university')
plt.plot(x2, y2, 'g', marker='s', label='Harvard university')
plt.plot(x2, y3, 'b', marker='o', label='University of Cambridge')
plt.xlabel('Year')
plt.ylabel('Rank')
plt.legend()
plt.show()

# %%
# No of universities from each country for each year
data.groupby(['year', 'country'])['institution'].nunique()

# %%
# rank of all the institutions in 2014
data[data['year'] == 2014][['institution', 'world_rank']]

# %%
# How many universities got same rank in both world and national_
data[data['world_rank'] == data['national_rank']].shape[0]

# %%
# Top five universities in USA
data[data['country'] == 'USA'][['institution', 'national_rank']].head()

# %%
# no of students leaving out in 2014 in different countries
data10 = data[data['year'] == 2014]
data11 = data10.groupby('country')['citations'].sum()
plt.xticks(rotation=90)
sns.set(font_scale=0.4)
sns.barplot(x=data11.index, y=data11.values)
plt.title('no of students leaving out in 2014')
plt.ylabel('count')

# %%
# average quality of education in each institution  different years
data12 = data.groupby('institution')['quality_of_education'].mean()
data13 = data12.sort_values().head(30)
data13.plot(kind='bar')

# %%
# Relationship between quality of education and quality of faculty
sns.set(font_scale=1)
(sns.lmplot(x='quality_of_faculty', y='quality_of_education',
            data=data, fit_reg=False, hue='year'))
plt.title('Relationship between education and faculty')
