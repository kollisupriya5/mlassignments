"""Loading and checking nulls for ipl data set."""
# %%
import pandas as pd
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
data = pd.read_csv('~/data/matches.csv')
data.head()
data.info()

# %%df.values
# drop umpire3 column
data.isnull().sum()
data = data.drop(['umpire3'], axis='columns')

# %%
# barplot for no of playerof match awards for each player
d1 = data['player_of_match'].value_counts().head(10)
d1
plt.xticks(rotation=90)
plt.yticks([0, 2, 4, 6, 8, 10, 12, 14, 16, 18])
x = sns.barplot(x=d1.index, y=d1.values)

# %%
# total no of matches played by each team
match = data['team1'].value_counts() + data['team2'].value_counts()
# barplot for total no of matches played by each team
plt.xticks(rotation=90)
sns.barplot(x=match.index, y=match.values)

# %%
# total no of matches played by eact teeam in the year 2017
data2017 = data[data['season'] == 2008]
data2017['team1'].value_counts() + data2017['team2'].value_counts()

# %%
# no of matches won by each team
df = data['winner'].value_counts()
df
per = (df/match)*100
ipl = pd.DataFrame()
ipl['matches'] = match
ipl['matches_win'] = df
ipl['win_per'] = per
ipl.sort_values(by='win_per', ascending=False)


# %%
def first_played(df):
    """Function."""
    if df['toss_decision'] == 'bat':
        return df['toss_winner']
    else:
        if df['toss_winner'] != df['team1']:
            return df['team1']
        else:
            return df['team2']


data1 = data
data1['bat_first_team'] = data.apply(first_played, axis=1)
data1.head()


# %%
# add a column to indicate whether a winning team batted or fielded first

data1['bat_fielding'] = (np.where(data1['bat_first_team'] == data1['winner'],
                                  'bat', 'field'))
data1.head(10)

# %%
# display the win count when batting first vs fielding first_played
data1['bat_fielding'].value_counts()

# %%
# calculate ratio of total wins will batting first or fielding first_played
data3 = data1['bat_fielding'].value_counts()
data3/len(data1)

# %%
# plot of win count when batting first vs fielding first_played
sns.barplot(x=data3.index, y=data3.values)

# %%
# plot of win count for each team when batting first or fielding first_played
data4 = data1['team1'].value_counts() + data1['team2'].value_counts()
data5 = data.groupby(['winner'])['bat_fielding'].value_counts()
data6 = pd.DataFrame(data5)
data6.rename(columns={'bat_fielding': 'wins'}, inplace=True)
data6.reset_index(level=1, inplace=True)
data6
plt.xticks(rotation=90)
sns.barplot(x=data6.index, y=data6.wins, hue=data6.bat_fielding)

# %%
# no of matches played by each team per season
da = data.groupby(['season'])['team1'].value_counts()
da
da1 = data.groupby(['season'])['team2'].value_counts()
da1
new_index = da.index.set_names(['season', 'team'])
da.index = new_index
da
new_index1 = da1.index.set_names(['season', 'team'])
da1.index = new_index1
da1
da+da1

# %%
# no of wins of each each team each season
data1.head()
data.groupby(['winner'])['season'].value_counts()

# %%
# No of matches played in hyderabad
match_hyd = data[data['city'] == 'Hyderabad']
match1 = match_hyd['team1'].value_counts() + match_hyd['team2'].value_counts()
match1

# %%
# which city each team won more matches
data['city']
data['winner']
data.groupby('winner')['city'].value_counts()

# %%
# most no matches played in which city
data['city'].value_counts()

# %%
# no of matches win by wickets for each team
data8 = data[data['win_by_wickets'] > 0]
data8['winner'].value_counts().sort_values(ascending=False)

# %%
# no of matches are tie
data7 = data[data['result'] == 'tie']
data7['result'].count()
