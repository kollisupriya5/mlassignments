"""Ipl dataset."""
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

# %%
# Loading the data
matches_data = pd.read_csv('~/data/matches.csv')
matches_data.head()
deliveries_data = pd.read_csv('~/data/deliveries.csv')
deliveries_data.head()

# %%
# No of matches per season
ax = sns.countplot(x='season', data=matches_data)

# %%
# most no of wins per each team
plt.xticks(rotation=90)
sns.countplot(x='winner', data=matches_data)

# %%
# Total no of runs in every season
matches_data.head()
df1 = matches_data[['id', 'season']]
df2 = deliveries_data[['match_id', 'total_runs']]
totalruns = pd.merge(df1, df2, left_on='id', right_on='match_id')
df3 = totalruns.groupby('season')['total_runs'].sum()
sns.barplot(x=df3.index, y=df3.values)
plt.ylabel('total_runs')
plt.title('Total no of runs in every season')

# %%
# team having maximum no of boundaries
b_6 = deliveries_data[deliveries_data['batsman_runs'] == 6]
sixes = b_6['batting_team'].value_counts().reset_index()
fours = (deliveries_data[deliveries_data['batsman_runs'] == 4]
                        ['batting_team'].value_counts()).reset_index()
boundaries = pd.merge(sixes, fours, left_on='index', right_on='index')
boundaries = boundaries.set_index('index')
boundaries.plot(kind='bar')

# %%
# Top empires of all the matches
matches_data = matches_data.drop(['umpire3'], axis='columns')
matches_data['umpire1'].value_counts()
matches_data['umpire2'].value_counts()
umpire1 = matches_data['umpire1']
umpire2 = matches_data['umpire2']
umpires = pd.concat([umpire1, umpire2]).value_counts().head(10)
sns.barplot(y=umpires.index, x=umpires.values, palette='winter')
plt.xlabel('count')
plt.ylabel('umpire_name')

# %%
# Final ipl winner for each season
matches_data.head()
winner = matches_data[['season', 'winner']]
winner.drop_duplicates(subset=['season'], keep='last').sort_values(by='season')

# %%
# Player with most boundaries
b = deliveries_data[(deliveries_data['batsman_runs'] == 6) | (deliveries_data['batsman_runs'] == 4)]
m_b = b['batsman'].value_counts().head(10)
m_b.plot(kind='bar')
plt.xlabel('batsman')
plt.ylabel('batsman runs')
plt.title('Player with more no of boundaries')

# %%
# No of matches played at each venue
matches_data.head()
venue = matches_data['venue'].value_counts().head(10)
plt.xticks(rotation=90)
sns.barplot(x=venue.index, y=venue.values)
plt.xlabel('venue')
plt.ylabel('count')
plt.title('No of matches played at each venue')

# %%
# Batsman with highest no of runs each season
df1 = matches_data[['id', 'season']]
df2 = deliveries_data[['batsman', 'batsman_runs', 'match_id']]
runs = pd.merge(df1, df2, left_on='id', right_on='match_id')
highest_runs = runs.groupby(['season', 'batsman'])['batsman_runs'].sum()
highest_runs = highest_runs.reset_index()
highest_runs = highest_runs.sort_values(by=['batsman_runs'], ascending=False)
batsman = highest_runs.drop_duplicates(subset=['season'], keep='first').sort_values(by='season')
batsman.plot(['batsman', 'season'], 'batsman_runs', color='gray', kind='bar')
plt.ylabel('count')
plt.title('Batsman with highest no of runs in each season')

# %%
# Which team had win by maximum runs and maximum wickets
team = matches_data.groupby('winner')['win_by_runs'].max()
team.sort_values(ascending=False)
team1 = matches_data.groupby('winner')['win_by_wickets'].max()
team1.sort_values(ascending=False)

# %%
# which team had win by minimum  of runs
team = matches_data[matches_data['win_by_runs'] != 0]
team1 = team.groupby('winner')['win_by_runs'].min()
team1.sort_values(ascending=True)

# %%
# Man of the match in final match
winner = matches_data.drop_duplicates(subset=['season'], keep='last')
man_of_match = winner[['winner', 'player_of_match']].reset_index(drop=True)
man_of_match

# %%
# No of ipl seasons won by each team
winner['winner'].value_counts()

# %%
# Percentage of toss decisions
toss = matches_data['toss_decision'].value_counts()
count = matches_data['toss_decision'].count()
percentage = (toss/count) * 100
percentage

# %%
# Toss decision across the season
sns.countplot(x='season', hue='toss_decision', data=matches_data)
plt.title('Toss decision across the season')

# %%
# Which team win the toss more times
toss = matches_data['toss_winner'].value_counts()
plt.xticks(rotation=90)
sns.barplot(x=toss.index, y=toss.values)
plt.xlabel('Team')
plt.ylabel('count')
plt.title('Team with winning more times of toss')

# %%
# No of wins by runs
matches_data.head()
matches_data[['win_by_runs', 'team1', 'team2']].sort_values(by='win_by_runs', ascending=False).head(10)

# %%
# Batsman dismissed in the first over itself
over = deliveries_data[deliveries_data['over'] == 1]
(over.groupby('player_dismissed')['batsman'].count()
     .sort_values(ascending=False).head(10))

# %%
# Batsman dismissed int first ball itself
ball = deliveries_data[deliveries_data['ball'] == 1]
(ball.groupby('player_dismissed')['batsman'].count()
     .sort_values(ascending=False).head(10))

# %%
# No of balls by each bowler for which the batsman got 0 runs
zero_runs = deliveries_data[deliveries_data['batsman_runs'] == 0]
zero_runs = zero_runs.groupby('bowler').size().sort_values(ascending=False).head(10)
plt.xticks(rotation=90)
sns.barplot(x=zero_runs.index, y=zero_runs.values)
plt.ylabel('No of balls')
plt.title('No of balls by each bowler for which the batsman got 0 runs')

# %%
# Run rate for each batsman
runs = deliveries_data.groupby('batsman')['total_runs'].sum()
balls = deliveries_data.groupby('batsman')['ball'].size()
run_rate = (runs/balls) * 100
run_rate.sort_values(ascending=False)

# %%
# Highest no of players dismissed by which dismissal kind
deliveries_data.groupby('dismissal_kind').size().sort_values(ascending=False)
