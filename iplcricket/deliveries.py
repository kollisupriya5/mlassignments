# %%
import pandas as pd
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
data = pd.read_csv('~/data/deliveries.csv')
data.head(5)
data.shape
data.isnull().sum()

# %%
# total no of matches first_played
data.columns
data['match_id'].nunique()

data['batting_team'].nunique()
pd.concat(data['batting_team'], data['bowler'], data['non_striker'], data['fielder'],ignore_index = True)

# %%
# total no of runs played by each player
data1 = data.groupby('batsman')['total_runs'].sum().sort_values(ascending=False)
data2 = data1.head(10)
data2
plt.xticks(rotation=90)
sns.barplot(x=data2.index,y=data2.values)

# %%
# Bowlers who gives more wide runs
data.groupby('bowler')['wide_runs'].sum().sort_values(ascending=False)

# %%
# No of teams who completed 4 innings
data3 = data[data['inning'] == 4]
data3['batting_team'].count()

# %%
# no of balls with zero runs
data4 = data[data['total_runs'] == 0]
data4['total_runs'].count()

# %%
# batsman with higest run outs
data5 = data[data['dismissal_kind'] == 'run out']
data5['batsman'].value_counts()

# %%
# which team played super over and how many super overs it played
data6 = data[data['is_super_over'] > 0]
data6['batting_team'].value_counts()
