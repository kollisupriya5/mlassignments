"""Loading and checking nulls for babay names data set."""
# %%
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
data = pd.read_csv('~/data/NationalNames.csv')
data.head()
data.info()
data.isnull().sum()
data.nunique()
data.shape

# %%
# No of children born in 1880
data1 = data[data['Year'] == 1880]
data11 = data1['Gender'].value_counts()
data11
sns.barplot(x=data11.index, y=data11.values)

# %%
# percentage of male and female in the population
data2 = data['Gender'].value_counts()
data2/len(data)
sns.barplot(x=data2.index, y=data2.values)

# %%
# no of the people whose name is marry
data3 = data[data['Name'] == 'Mary']
data3['Gender'].count()
data3['Gender'].value_counts()

# %%
# Least occurance of names
data6 = data.groupby('Name')['Count'].sum().sort_values(ascending=False)
data6.tail(5)

# %%
# Top most popular names
data6 = data.groupby('Name')['Count'].sum().sort_values(ascending=False)
data6.head()

# %%
# no of babies born each year
data7 = data.groupby('Year')['Count'].sum()
data7

# %%
# different names for every year(plot)
data8 = data.groupby('Year')['Name'].nunique().head(10)
sns.barplot(x=data8.index, y=data8.values)

# %%
# top female names in the year 2010
data9 = data[data['Year'] == 2010]
data10 = data9[data9['Gender'] == 'F']
data10
data11 = data10.groupby('Name')['Count'].sum().sort_values(ascending=False)
data11.head()
