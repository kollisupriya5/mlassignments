"""Election dataset."""

# %%
#loading and checkong nulls
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
data = pd.read_csv('~/data/election.csv')
data.head(20)
data.info()
data.shape
data.isnull().sum()

# %%
# range for contributed receipt amount and receipt date
data.nunique()
data['contb_receipt_amt'].min()
data['contb_receipt_amt'].max()
data['contb_receipt_dt'].min()
data['contb_receipt_dt'].max()

# %%
# barplot for highest amount of contribution for the candidate
data2 = (data.groupby(['cand_nm'])['contb_receipt_amt'].sum().sort_values(ascending=False))
plt.xticks(rotation=90)
sns.barplot(x=data2.index, y=data2.values)

# %%
# No of candidates contributed by the alaska state
data2 = data[data['contbr_st'] == 'AL']
data2['cand_nm'].nunique()

# %%
# total nof candidates participated
data['cand_id'].nunique()

# %%
# no of contributors to each candidate
data.groupby('cand_nm')['contbr_nm'].nunique()

# %%
# top 5 highest contributed states
data3 = data.groupby('contbr_st')['contb_receipt_amt'].sum().sort_values(ascending=False)
data3.head()

# %%
# no of contributors to each commitee
data.groupby('cmte_id')['contbr_nm'].nunique()
