"""Malasian election result dataset."""

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

# %%
# loading the data
data = pd.read_csv('~/data/malasian_election.csv')
data.head()
data.isnull().sum()
data.nunique()
data['Coalition'].unique()
data['Candidate Party'].unique()

# %%
# Total Number of votes cast for each coalition party
data.groupby('Coalition')['Total Votes Cast'].sum()


# %%
# No of candidates participated per Gender
data2 = data['Gender'].value_counts()
sns.barplot(x=data2.index, y=data2.values)
plt.xlabel('Gender')
plt.title('No of candidates participated per gender')

# %%
# No of candidates win per Gender
data3 = data[data['Candidate Win'] == 1]['Gender'].value_counts()
sns.barplot(x=data3.index, y=data3.values)
plt.xlabel('Gender')
plt.title('No of winning candidates per gender')

# %%
# Candidate who won with higest percentage of votes
data4 = data[data['Candidate Win'] == 1]
data5 = data4.groupby('Candidate Name')['% of total Votes'].sum()
data5.sort_values(ascending=False).head(1)

# %%
# Candidate who won with lowest percentage of votes
data6 = data4.groupby('Candidate Name')['% of total Votes'].sum()
data6.sort_values(ascending=False).tail(1)

# %%
# Total Number of seats won by each gender for each party
data7 = data4.groupby('Coalition')['Gender'].value_counts()
data7

# %%
# No of seats won by each coalition
data4.groupby('Coalition').size()

# %%
# candidate who got the highest no of votes who is stand for Padang besar seat
data9 = data[data['Seat Name'] == 'PADANG BESAR']
data9.groupby('Candidate Name')['Votes for Candidate'].sum().head(1)

# %%
# How many no of candidates nominated for each seat
data['Seat Name'].value_counts().sort_values(ascending=False).head()

# %%
# no of candidates participated when party and coaliation are same
data10 = (data['Coalition'] == data['Candidate Party'])
data10.value_counts()[True]

# %%
# Percentage of seats win when both party and coalition are same
data11 = data[data['Coalition'] == data['Candidate Party']]
data12 = data11[data11['Candidate Win'] == 1]['Candidate Win'].count()
data13 = data['Seat Name'].nunique()
per = (data12/data13)*100
per

# %%
# No of candidtes are nominated whose job is Tiada
data[data['Pekerjaan'] == 'TIADA']['Pekerjaan'].count()

# %%
# candidates belongs to which occupation wins more seats
data4.groupby('Pekerjaan').size().sort_values(ascending=False).head()

# %%
# Candidate who won with higest no of votes

data14 = data4.groupby('Candidate Name')['Votes for Candidate'].sum()
data14.sort_values(ascending=False).head(1)

# %%
# Candidate who lose with highest no of percentage of votes
data15 = data[data['Candidate Win'] == 0]
data16 = data15.groupby('Candidate Name')['% of total Votes'].sum()
data16.sort_values(ascending=False)

# %%
# Top parties through which most of the candidates participated
data.groupby('Candidate Party').size().sort_values(ascending=False).head(5)

# %%
# Proportion of males and females participated in election
data17 = data['Gender'].count()
proportion = data2/data17
proportion
