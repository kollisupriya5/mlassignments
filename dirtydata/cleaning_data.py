"""Cleaning dirty data."""

import pandas as pd
# Loading the data
data = pd.read_csv('~/data/movie_metadata.csv')
data.head(5)
data.shape
data.columns
data.isnull().sum()  # To check the count of nulls in each column
data['actor_2_name']  # To select particular column
data[['color', 'director_name']]  # To select multiple columns
data['color'][:10]  # To select first ten rows of a column
data['gross'].describe()

# Dealing with missing values
# Replace the missing values of the categorical data either with empty string.
# Or replace with any default name such as 'None Given'
data['country'] = data['country'].fillna('None Given')
data.isnull().sum()
data['country']

# For numerical columns we can replace with mean or medain of the column
data.duration = data['duration'].fillna(data['duration'].mean())
data.duration

# Remove incomplete rowsdata['title_year'].isnull().sum()
data['duration'].isnull().sum()
data.dropna()  # Dropping all rows with any NA values
data.dropna(how='all')  # drop rows that have all NA values
data.dropna(thresh=5)  # Drop the rows that have 5 NA values
data['title_year'].isnull().sum()
data.dropna(subset=['title_year']).isnull().sum()

# Deal with error-prone columns
data.dropna(axis=1, how='all').shape
data.dropna(axis=1, how='any').shape
data.shape
data.dropna(axis=1, thresh=5000).shape

# Normalize data types
data2 = pd.read_csv('~/data/movie_metadata.csv', dtype={'duration': int})
data3 = pd.read_csv('~/data/movie_metadata.csv', dtype={'title_year': str})

# Change casing
data['movie_title'].str.upper()
data['movie_title'].str.strip()

# Rename columns
data.rename(columns={'title_year': 'release_date', 'movie_facebook_likes': 'facebook_likes'})
data
data1 = data.rename(columns={'title_year': 'release_date', 'movie_facebook_likes': 'facebook_likes'})
data1
data1.to_csv('cleanfile.csv',encoding='utf-8')
