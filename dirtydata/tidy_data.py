"""Cleaning tidy data."""
import pandas as pd
import numpy as np

pew_data = pd.read_csv('~/data/pew-raw.csv')
pew_data

# %%
# Column headers are values, not variable names
pew_data_new = (pd.melt(pew_data, ['religion'],
                         var_name='income', value_name='freq'))
pew_data_new

# %%
billbord_data = pd.read_csv('~/data/billboard.csv', encoding='latin-1')
billbord_data.head(10)
billbord_data.columns
id_vars = ['year', 'artist.inverted', 'track', 'time', 'genre',
           'date.entered', 'date.peaked']
df = pd.melt(billbord_data, id_vars=id_vars, var_name='week', value_name='rank')
df
df['week'] = df['week'].str.extract('(\d+)', expand=False).astype(int)
df = df.dropna()
df
df['rank'] = df['rank'].astype(int)

# create date column
df['date'] = pd.to_datetime(df['date.entered'])+pd.to_timedelta(df['week'], unit='w')-pd.DateOffset(weeks=1)
df
df = df[['year', 'artist.inverted', 'track', 'time', 'genre', 'week', 'rank', 'date']]
df.sort_values(ascending=True, by = ["year","artist.inverted","track","week","rank"])
df

billbord = df
billbord

# %%
# Multiple types in one table
song_cols = ['year', 'artist.inverted', 'track', 'time', 'genre']
songs = billbord[song_cols].drop_duplicates()
songs
songs['song_id'] = songs.index
songs.head(10)

ranks = pd.merge(billbord, songs, on=['year', 'artist.inverted', 'track', 'time', 'genre'])
ranks = ranks[['song_id', 'date', 'rank']]
ranks

# Multiple variables sorted in one column
tb = pd.read_csv('~/data/tb-raw.csv')
tb
tb_tmp = pd.melt(tb, id_vars=['country', 'year'], value_name='cases', var_name="sex_and__age")
tb_tmp
# Extract sex age lower bound and age upper bound group
tmp_df = tb_tmp["sex_and__age"].str.extract("(\D)(\d+)(\d{2})")
tmp_df
tmp_df.columns = ['sex', 'age_lower', 'age_upper']
tmp_df

# # Create `age`column based on `age_lower` and `age_upper`
tmp_df['age'] = tmp_df['age_lower']+'-'+tmp_df['age_upper']
tmp_df

df = pd.concat([tb, tmp_df], axis=1)
df
