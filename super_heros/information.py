"""Super heroes dataset."""

import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt

# %%
# Loading the data
data = pd.read_csv('~/data/heroes_information.csv')
data.head(5)
data.shape
data.isnull().sum()
data.nunique()

# %%
# Total no of superheroes
data['name'].nunique()

# %%
# plot for Which comic has the highest no of super-heroes
data['Publisher'].nunique()
data1 = data['Publisher'].value_counts()
plt.xticks(rotation=90)
sns.barplot(x=data1.index, y=data1.values)

# %%
# plot for no of male,female and others in the dataset
data.Gender[data.Gender == '-'] = 'other'
data['Gender']
data2 = data['Gender'].value_counts()
data2
sns.barplot(x=data2.index, y=data2.values)

# %%
# No of heroes with gray colour eyes
data[data['Eye color'] == 'grey'].shape[0]

# %%
# Superheroes with no Hair
data3 = data[data['Hair color'] == 'No Hair']
data3['name'].count()

# %%
# Avg height of super heroes
data['Height'].mean()

# %%
# average weight of the Superheroes
data['Weight'].mean()

# %%
# No of yellow eye color super heroes race with Human
data4 = data[data['Eye color'] == 'yellow']
data4[data4['Race'] == 'Human'].shape[0]

# %%
# plot for top 20 Race types of super heroes
data5 = data['Race'].value_counts().head(20)
plt.xticks(rotation=90)
sns.barplot(x=data5.index, y=data5.values)

# %%
# people with no hair and red colured skin
data6 = data[data['Hair color'] == 'No Hair']
data7 = data6[data6['Skin color'] == 'red']
data7['Gender'].value_counts()

# %%
# plot for Alignment of male and female super heroes
data8 = data[data['Gender'] == 'Female']
data9 = data8['Alignment'].value_counts()
sns.barplot(x=data9.index, y=data9.values)
plt.title('alignment for Female')

data.Alignment[data.Alignment == '-'] = 'other'
data10 = data[data['Gender'] == 'Male']
data11 = data10['Alignment'].value_counts()
sns.barplot(x=data11.index, y=data11.values)
plt.title('Alignment for Male')

# %%
# Hair color, skin color and eye color distibution of Superheroes
data12 = data[data['Hair color'] != 'No Hair']
data13 = data12['Hair color'].value_counts()
data14 = data[data['Skin color'] != '-']['Skin color'].value_counts()
data15 = data[data['Eye color'] != '-']['Eye color'].value_counts()
data15

plt.xticks(rotation=90)
sns.barplot(x=data13.index, y=data13.values)
plt.title('Hair color distibution of super heroes')

plt.xticks(rotation=90)
sns.barplot(x=data14.index, y=data14.values)
plt.title('Skin color distibution of super heroes')

plt.xticks(rotation=90)
sns.barplot(x=data15.index, y=data15.values)
plt.title('Eye color distibution of super heroes')

# %%
# Superheroes whose weight is greater than 300 based on gender
data[data['Weight'] >= 300]['Gender'].value_counts()

# %%
# Top 5 tallest super heroes
data.groupby('name')['Height'].sum().sort_values(ascending=False).head()

# %%
# Top 5 superheroes in Weight
data.groupby('name')['Weight'].sum().sort_values(ascending=False).head()

# %%
# Percentage of alignments
df = data['Alignment'].value_counts()
per = (df/len(data))*100
per

# %%
# barplot indicating the no of publications of each publisher
data16 = data.groupby('Publisher').size().sort_values(ascending=False)
plt.xticks(rotation=90)
sns.barplot(x=data16.index, y=data16.values)

# %%
# which superhero has got highest no of publications
data17 = data.groupby('name')['Publisher'].count().sort_values(ascending=False)
data17.head()
