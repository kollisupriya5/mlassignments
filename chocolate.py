"""Chocolate bar ratings dataset."""

import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

# %%
# loading the data
data = pd.read_csv('~/data/chocolate.csv')
data.head(10)
data.isnull().sum()
data.shape
data.nunique()

# Rename the columns
original_colnames = data.columns
new_colnames = ['company', 'Bean_origin', 'REF', 'review_year',
                'cocoa_percentage', 'company_location', 'rating',
                'bean_type', 'country']
data = data.rename(columns=dict(zip(original_colnames, new_colnames)))
data.nunique()

# %%
# No of companies manufacturing the chocolate bars
data['company'].nunique()

# %%
# no of different types of bars considered
data['Bean_origin'].nunique()

# %%
# How many companies located in each country
df3 = data['company_location'].value_counts()
sns.set(font_scale=0.5)
sns.set(rc={'figure.figsize': (11.7, 8.27)})
sns.barplot(x=df3.values, y=df3.index)

# %%
# Where are the best cocoa beans grown?
(data.groupby('Bean_origin')['cocoa_percentage'].count()
     .sort_values(ascending=False))


# %%
# No of types of chocolate bars prepared by each company
df = (data.groupby('company')['Bean_origin'].count()
          .sort_values(ascending=False)).head(10)
plt.xticks(rotation=90)
sns.barplot(x=df.index, y=df.values)

# %%
# Which countries produce the highest-rated bars?
df1 = (data.groupby('country')['rating'].count()
           .sort_values(ascending=False)).head(20)
plt.xticks(rotation=90)
sns.barplot(x=df1.index, y=df1.values)

# %%
# List of chocolates whose rating is equal to 5
data1 = data[data['rating'] == 5]
data1['Bean_origin'].values

# %%
# companies whose rating is less than 2
data[data['rating'] < 2]['company']

# %%
# Howmany times each company reviewed in 2014
data2 = data[data['review_year'] == 2014]
data2['company'].value_counts().sort_values(ascending=False)

# %%
# 5 Mostly rated chocolte in 2012
data3 = data[data['review_year'] == 2012]
(data3.groupby('Bean_origin')['rating'].sum()
      .sort_values(ascending=False)).head()

# %%
# no of companies got reviewd each year
data.groupby('review_year')['company'].count()

# %%
# Howmany times each company reviewed each year
data.groupby('review_year')['company'].value_counts()

# %%
# plot of rating score for companies located in U.S.A.
df4 = data[data['company_location'] == 'U.S.A.']
df4
df5 = df4['rating']
plt.hist(df5, bins=20)

# %%
# Most recently added choco entries with name, company and reference value(REF)
data.sort_values(by='REF', ascending=False)[['Bean_origin', 'company', 'REF']].head()

# %%
# Howmany companies use the beans of type blend
data['bean_type'].unique()
data4 = data[data['bean_type'] == 'Blend']
data4['company'].count()

# %%
# average rating for each company
data.groupby('company')['rating'].mean()
