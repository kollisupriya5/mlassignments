"""Top 500 Indian Cities."""

import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

# %%
# Loading the data
data = pd.read_csv('~/data/Indian_cities.csv')
data.head()
data.isnull().sum()
data.nunique()

# %%
# No of states in Indian
data['state_name'].nunique()

# %%
# No of districts for each state
data.head()
d_S = (data.groupby('state_name')['dist_code'].count()
           .sort_values(ascending=False))
d_S.plot(kind='bar')
plt.ylabel('District_count')
plt.title('No of districts for each state')

# %%
# No of cities in each state
cities = (data.groupby('state_name')['name_of_city'].count()
              .sort_values(ascending=False))
cities.plot(kind='bar')
plt.ylabel('Total')
plt.title('No of cities in each state')

# %%
# Total population of each state
pop_s = (data.groupby('state_name')['population_total'].sum()
             .sort_values(ascending=False))
plt.xticks(rotation=90)
plt.ylabel('Population')
plt.title('Total population in each state')
sns.barplot(x=pop_s.index, y=pop_s.values)

# Plot for Population of  Male Vs Female in each state?
mpop_s = (data.groupby('state_name')['population_male'].sum()
              .sort_values(ascending=False))
fpop_s = (data.groupby('state_name')['population_female'].sum()
              .sort_values(ascending=False))

plt.subplot(1, 2, 1)
plt.xticks(rotation=90)
sns.set(font_scale=0.4)
sns.barplot(x=mpop_s.index, y=mpop_s.values)
plt.ylabel('population')
plt.title('Population of male in each state')

plt.subplot(1, 2, 2)
plt.xticks(rotation=90)
sns.barplot(x=fpop_s.index, y=fpop_s.values)
plt.ylabel('population')
plt.title('Population of female in each state')

# %%
# Percentage of male population in each state
m_pop = data.groupby('state_name')['population_male'].sum()
m_per = (m_pop/pop_s)*100
m_per_sort = m_per.sort_values(ascending=False)
m_per_sort.plot(kind='bar')
plt.ylabel('Percentage')
plt.title('Percentage of male population in each state')

# %%
# Percentage of female population in each state
f_pop = data.groupby('state_name')['population_female'].sum()
f_per = (f_pop/pop_s)*100
f_per_sort = f_per.sort_values(ascending=False)
f_per_sort.plot(kind='bar')
plt.ylabel('Percentage')
plt.title('Percentage of female population in each state')

# %%
# Total graduates percentage of  each state in entire population
s_gra = data.groupby('state_name')['total_graduates'].sum()
s_gra1 = s_gra.sort_values(ascending=False)
tg_pop = (s_gra1/pop_s)*100
tg_pop_sort = tg_pop.sort_values(ascending=False)
tg_pop_sort.plot(kind='bar')
plt.ylabel('Graduates percentage')
plt.title('States by total graduates percentage in entire population')

# %%
# Male garaduates percentage in male population
m_gra = data.groupby('state_name')['male_graduates'].sum()
per = (m_gra/m_pop)*100
plt.xticks(rotation=90)
plt.plot(per, marker='o')
plt.ylabel('percentage')
plt.xlabel('state name')
plt.title('Male graduates percentage in male population')

# %%
# Female graduate percentage in  female population
f_gra = data.groupby('state_name')['female_graduates'].sum()
per = (f_gra/f_pop)*100
plt.xticks(rotation=90)
plt.plot(per, marker='o')
plt.ylabel('percentage')
plt.xlabel('state name')
plt.title('Female graduates in female population')

# %%
# Top 50 cities in the country by population
c_pop = (data.groupby('name_of_city')['population_total'].sum()
             .sort_values(ascending=False)).head(50)
plt.xticks(rotation=90)
sns.set(font_scale=0.65)
plt.ylabel('Population')
sns.barplot(x=c_pop.index, y=c_pop.values)
plt.title('Top 50 cities in the country by population')

# %%
# Male literacy rate vs female literacy rate
(sns.lmplot(x='effective_literacy_rate_male',
            y='effective_literacy_rate_female',
            hue='state_name', data=data, fit_reg=False))
plt.title('Male literacy rate VS female literacy rate')

# %%
# Male graduate rate vs female graduate rate
(sns.lmplot(x='male_graduates', y='female_graduates',
            data=data, hue='state_name', fit_reg=False))
plt.xticks(rotation=90)
plt.title('Male graduate rate VS female graduate rate')

# %%
# Top 10 cities with most number of literates
c_lit = (data.groupby('name_of_city')['literates_total'].sum()
             .sort_values(ascending=False)).head(10)
c_lit

# %%
# Percentage of female literates in each state
f_lit = data.groupby('state_name')['literates_female'].sum()
f_lit.sort_values(ascending=False)
s_lit = data.groupby('state_name')['literates_total'].sum()
per = (f_lit/s_lit)*100
per.sort_values(ascending=False)

# %%
# How many male and female literates are in each state?
f_lit_sort = f_lit.sort_values(ascending=False)
m_lit = data.groupby('state_name')['literates_male'].sum()
m_lit_sort = m_lit.sort_values(ascending=False)
plt.xticks(rotation=90)
plt.plot(f_lit_sort, label='female')
plt.plot(m_lit_sort, label='male')
plt.legend()
plt.ylabel('Total no of literates')
plt.title('Total no of literates(Male VS Female)')

# %%
# Top 10 cities with high literacy Rate
sns.set(font_scale=1)
c_lit_rate = (data.groupby('name_of_city')['effective_literacy_rate_total']
                  .sum().sort_values(ascending=False)).head(10)
c_lit_rate.plot(kind='bar')
plt.ylabel('Literacy Rate')
plt.title('Top 10 cities with high literacy rate')

# %%
# Sex ratio for each state
sns.set(font_scale=3)
fig = plt.figure(figsize=(10, 15))
sex_ratio = (data.groupby('state_name')['sex_ratio'].mean()
                 .sort_values(ascending=False))
sex_ratio.plot(kind='bar', fontsize=15)
plt.ylabel('Ratio')
plt.title('Sex ratio for each state')

# %%
# Top 10 cities with high kid population
c_kidpop = data.groupby('name_of_city')['0-6_population_total'].sum()
c_kidpop.sort_values(ascending=False).head(10)

# %%
# Female kids population in each state
s_fkidpop = data.groupby('state_name')['0-6_population_female'].sum()
s_fkidpop.sort_values(ascending=False)

# %%
# Toatl kids population in each city
f_fkidpop = data.groupby('name_of_city')['0-6_population_total'].sum()
f_fkidpop.sort_values(ascending=False)

# %%
# Relation between sex ratio and child sex Ratio
sns.set(font_scale=1)
(sns.lmplot(x='sex_ratio', y='child_sex_ratio', hue='state_name',
            data=data, fit_reg=False))
plt.title('Sex ratio vs child sex ratio')

# %%
# Relationship between sex ratio and gardutes
(sns.lmplot(x='sex_ratio', y='total_graduates', hue='state_name',
            data=data, fit_reg=False))
plt.title('sex ratio VS graduates')
