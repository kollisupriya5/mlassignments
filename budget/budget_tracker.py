"""Budget tracker."""


# %%
def main():
    """Menu selection and functionality."""
    choice = 0
    Initial_Balance = int(input('Enter your Initial amount: '))
    total_budget = [('+'+str(Initial_Balance), 'salary')]
    while choice != '6':
        print()
        print('Initial Balance: Rs', Initial_Balance)
        print('Menu Selection')
        print('1.Initial Balance')
        print('2.Add Earnings')
        print('3.Add expenses')
        print('4.See current balance')
        print('5.Transaction History')
        print('6.Exit')
        print()
        choice = int(input('Enter your choice: '))
        if choice == 1:
            print('Initial balance in account is: Rs', Initial_Balance)
        elif choice == 2:
            total_budget = add_earnings(total_budget)
        elif choice == 3:
            total_budget = add_expenses(total_budget)
        elif choice == 4:
            print('Your Current balance is: Rs', end=' ')
            total_balance(total_budget)
        elif choice == 5:
            transactions(total_budget)
        elif choice == 6:
            print('Thank you for using Budget tracker')
            break
        else:
            print('Invalid selection, please enter 1-5')


def add_earnings(total_budget):
    """Adding earnings."""
    add_income = (int(input('Enter your income: Rs ')))
    reason = input('Enter by which purpose: ')
    total_budget.append(('+'+str(add_income), reason))
    return total_budget


def add_expenses(total_budget):
    """Adding expenses."""
    expenses = int(input('Enter your expense amount: Rs '))
    purpose = input('Enter for which purpose: ')
    total_budget.append(('-'+str(expenses), purpose))
    return total_budget


def total_balance(total_budget):
    """Total balance."""
    sum = 0
    for i in total_budget:
        sum = sum + int(i[0])
    print(sum)


def transactions(total_budget):
    """Summary of transactions."""
    for i, j in total_budget:
        print(i, j)


main()
